#include <iostream>
#include <math.h> 
using namespace std;
 
int main()
{
    setlocale(0,"");
 
    double a, eps, xn, xn1 = 1;
 
    cout << "Вычисление квадратного корня\n" << endl;
    cout << "Число:    "; 
    cin >> a;
    cout << "Точность: "; 
    cin >> eps;
 
    do {xn = xn1; xn1 = (xn + a/xn)/2;} while (fabs(xn1-xn) >= eps);
 
    cout << endl << "  sqrt(" << a << ") = " << xn1 << endl << endl;
 
    return 0;
}